package net.crytec.velocitycore;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import net.crytec.velocitycore.configuration.file.YamlConfiguration;

public enum ConfigOptions {
  PROTOCOL_VERSION("server.protocol", 575),
  PROTOCOL_VERSION_NAME("server.protocolname", "Minecraft 1.15.2"),

  MAX_PLAYERS("server.playerlimit", 50),

  TABLIST_ENABLED("tablist.enabled", true),
  TABLIST_FORMAT("tablist.format", "%prefix% | %playername%"),

  LOBBY_SERVER("server.lobby", "lobby"),

  MOTD("options.motd", Arrays.asList("Zeile 1", "Zeile 2")),

  MYSQL_USERNAME("mysql.username", "root"),
  MYSQL_PASSWORD("mysql.password", "password123"),
  MYSQL_DATABASE("mysql.database", "MyMinecraftDatabase"),
  MYSQL_HOST("mysql.host", "localhost"),
  MYSQL_PORT("mysql.port", 3306),
  ;

  private final String path;
  private final Object def;

  private static YamlConfiguration CONFIG;
  private static File file;

  private ConfigOptions(final String path, final Object object) {
    this.path = path;
    def = object;
  }

  public boolean asBoolean() {
    return CONFIG.getBoolean(path, (boolean) def);
  }

  public double asDouble() {
    return CONFIG.getDouble(path, (double) def);
  }

  public int asInt() {
    return CONFIG.getInt(path, (int) def);
  }

  public float asFloat() {
    return (float) CONFIG.getDouble(path, (float) def);
  }

  public String asString() {
    return CONFIG.getString(path, (String) def);
  }

  public List<String> asStringList() {
    return CONFIG.getStringList(path);
  }

  public void set(final Object value) {
    CONFIG.set(path, value);
  }

  public static YamlConfiguration getFile() {
    return CONFIG;
  }

  public static void save() {
    getFile().save(file);
  }

  /**
   * Get the default value of the path.
   *
   * @return The default value of the path.
   */
  public Object getDefault() {
    return def;
  }

  /**
   * Get the path to the string.
   *
   * @return The path to the string.
   */
  public String getPath() {
    return path;
  }

  private static boolean isValidPath(final String path) {
    return Arrays.stream(values()).anyMatch(lang -> lang.getPath().equals(path));
  }

  public static void initialize(final VelocityCore plugin) throws IOException {
    file = new File(plugin.getDataFolder(), "config.yml");
    if (!file.exists() && !file.createNewFile()) {
      plugin.getLogger().error("Failed to create config.yml");
      return;
    }

    final YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);
    CONFIG = configuration;

    int updated = 0;
    for (final ConfigOptions entry : values()) {
      if (!configuration.isSet(entry.getPath())) {
        configuration.set(entry.getPath(), entry.getDefault());
        updated++;
      }
    }

    if (updated > 0) {
      configuration.save(file);
      plugin.getLogger().info("Updated config.yml with " + updated + " new entries!");
    }
  }
}
