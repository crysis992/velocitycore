package net.crytec.velocitycore;

import com.google.gson.JsonObject;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.proxy.server.RegisteredServer;
import java.util.Optional;
import net.crytec.libs.redishelper.NetworkPacket;
import net.crytec.libs.redishelper.RedisManager;

public class PacketServerID extends NetworkPacket {

  protected static ProxyServer server;
  protected static RedisManager manager;

  private String servername;
  private int port;

  public PacketServerID() {
  }

  private PacketServerID(final String servername, final int port) {
    this.servername = servername;
    this.port = port;
  }

  @Override
  public void onPacketReceive(final JsonObject data) {
    final int port = data.get("port").getAsInt();

    final Optional<RegisteredServer> subServer =
        server.getAllServers().stream()
            .filter(server -> server.getServerInfo().getAddress().getPort() == port)
            .findFirst();
    subServer.ifPresent(
        registeredServer ->
            manager.broadcast(
                new PacketServerID(registeredServer.getServerInfo().getName(), port)));
  }

  @Override
  public void onPacketSent(final JsonObject data) {
    data.addProperty("servername", servername);
    data.addProperty("port", port);
    System.out.println(
        "Server " + servername + " is requesting Identification, sending servername..");
  }
}
