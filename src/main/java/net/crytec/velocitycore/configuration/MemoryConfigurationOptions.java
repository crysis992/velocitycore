package net.crytec.velocitycore.configuration;

import org.jetbrains.annotations.NotNull;

/**
 * Various settings for controlling the input and output of a {@link MemoryConfiguration}
 */
public class MemoryConfigurationOptions extends ConfigurationOptions {

  protected MemoryConfigurationOptions(@NotNull final MemoryConfiguration configuration) {
    super(configuration);
  }

  @NotNull
  @Override
  public MemoryConfiguration configuration() {
    return (MemoryConfiguration) super.configuration();
  }

  @NotNull
  @Override
  public MemoryConfigurationOptions copyDefaults(final boolean value) {
    super.copyDefaults(value);
    return this;
  }

  @NotNull
  @Override
  public MemoryConfigurationOptions pathSeparator(final char value) {
    super.pathSeparator(value);
    return this;
  }
}
