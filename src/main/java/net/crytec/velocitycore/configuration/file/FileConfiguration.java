package net.crytec.velocitycore.configuration.file;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import net.crytec.velocitycore.configuration.Configuration;
import net.crytec.velocitycore.configuration.InvalidConfigurationException;
import net.crytec.velocitycore.configuration.MemoryConfiguration;
import org.apache.commons.lang.Validate;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * This is a base class for all File based implementations of {@link Configuration}
 */
public abstract class FileConfiguration extends MemoryConfiguration {

  /**
   * Creates an empty {@link FileConfiguration} with no default values.
   */
  public FileConfiguration() {
    super();
  }

  /**
   * Creates an empty {@link FileConfiguration} using the specified {@link Configuration} as a source for all default values.
   *
   * @param defaults Default value provider
   */
  public FileConfiguration(@Nullable final Configuration defaults) {
    super(defaults);
  }

  /**
   * Saves this {@link FileConfiguration} to the specified location.
   *
   * <p>If the file does not exist, it will be created. If already exists, it will be overwritten.
   * If it cannot be overwritten or created, an exception will be thrown.
   *
   * <p>This method will save using the system default encoding, or possibly using UTF8.
   *
   * @param file File to save to.
   * @throws IllegalArgumentException Thrown when file is null.
   */
  public void save(@NotNull final File file) {
    Validate.notNull(file, "File cannot be null");

    final String data = saveToString();

    try (final Writer writer = new OutputStreamWriter(new FileOutputStream(file), Charsets.UTF_8)) {
      Files.createParentDirs(file);
      writer.write(data);
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Saves this {@link FileConfiguration} to a string, and returns it.
   *
   * @return String containing this configuration.
   */
  @NotNull
  public abstract String saveToString();

  /**
   * Loads this {@link FileConfiguration} from the specified location.
   *
   * <p>All the values contained within this configuration will be removed, leaving only settings
   * and defaults, and the new values will be loaded from the given file.
   *
   * <p>If the file cannot be loaded for any reason, an exception will be thrown.
   *
   * @param file File to load from.
   * @throws FileNotFoundException         Thrown when the given file cannot be opened.
   * @throws IOException                   Thrown when the given file cannot be read.
   * @throws InvalidConfigurationException Thrown when the given file is not a valid Configuration.
   * @throws IllegalArgumentException      Thrown when file is null.
   */
  public void load(@NotNull final File file)
      throws FileNotFoundException, IOException, InvalidConfigurationException {
    Validate.notNull(file, "File cannot be null");

    final FileInputStream stream = new FileInputStream(file);

    load(new InputStreamReader(stream, Charsets.UTF_8));
  }

  /**
   * Loads this {@link FileConfiguration} from the specified reader.
   *
   * <p>All the values contained within this configuration will be removed, leaving only settings
   * and defaults, and the new values will be loaded from the given stream.
   *
   * @param reader the reader to load from
   * @throws IOException                   thrown when underlying reader throws an IOException
   * @throws InvalidConfigurationException thrown when the reader does not represent a valid Configuration
   * @throws IllegalArgumentException      thrown when reader is null
   */
  public void load(@NotNull final Reader reader) throws IOException, InvalidConfigurationException {
    final BufferedReader input =
        reader instanceof BufferedReader ? (BufferedReader) reader : new BufferedReader(reader);

    final StringBuilder builder = new StringBuilder();

    try {
      String line;

      while ((line = input.readLine()) != null) {
        builder.append(line);
        builder.append('\n');
      }
    } finally {
      input.close();
    }

    loadFromString(builder.toString());
  }

  /**
   * Loads this {@link FileConfiguration} from the specified string, as opposed to from file.
   *
   * <p>All the values contained within this configuration will be removed, leaving only settings
   * and defaults, and the new values will be loaded from the given string.
   *
   * <p>If the string is invalid in any way, an exception will be thrown.
   *
   * @param contents Contents of a Configuration to load.
   * @throws InvalidConfigurationException Thrown if the specified string is invalid.
   * @throws IllegalArgumentException      Thrown if contents is null.
   */
  public abstract void loadFromString(@NotNull String contents)
      throws InvalidConfigurationException;

  /**
   * Compiles the header for this {@link FileConfiguration} and returns the result.
   *
   * <p>This will use the header from {@link #options()} -&gt; {@link
   * FileConfigurationOptions#header()}, respecting the rules of {@link FileConfigurationOptions#copyHeader()} if set.
   *
   * @return Compiled header
   */
  @NotNull
  protected abstract String buildHeader();

  @NotNull
  @Override
  public FileConfigurationOptions options() {
    if (options == null) {
      options = new FileConfigurationOptions(this);
    }

    return (FileConfigurationOptions) options;
  }
}
