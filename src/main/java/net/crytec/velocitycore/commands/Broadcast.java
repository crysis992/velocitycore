package net.crytec.velocitycore.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandIssuer;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import com.velocitypowered.api.proxy.ProxyServer;
import net.kyori.text.TextComponent;

@CommandAlias("broadcast|bcast")
@CommandPermission("velocity.broadcast")
public class Broadcast extends BaseCommand {

  private final ProxyServer server;

  public Broadcast(final ProxyServer server) {
    this.server = server;
  }

  @Default
  public void broadcast(final CommandIssuer sender, final String text) {
    server.broadcast(TextComponent.empty());
    server.broadcast(TextComponent.of(text.replace('&', '§')));
    server.broadcast(TextComponent.empty());
  }
}
