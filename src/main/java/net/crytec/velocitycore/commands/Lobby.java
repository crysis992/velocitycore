package net.crytec.velocitycore.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.proxy.server.RegisteredServer;
import com.velocitypowered.api.proxy.server.ServerPing;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import net.crytec.velocitycore.ConfigOptions;
import net.kyori.text.TextComponent;
import net.kyori.text.format.TextColor;

@CommandAlias("lobby|hub")
public class Lobby extends BaseCommand {

  private final ProxyServer server;

  public Lobby(final ProxyServer server) {
    this.server = server;
  }

  @Default
  public void execute(final Player issuer) {
    final Optional<RegisteredServer> lobby =
        server.getServer(ConfigOptions.LOBBY_SERVER.asString());

    if (lobby.isEmpty()) {
      issuer.sendMessage(
          TextComponent.of(
              "Fehler: Server "
                  + ConfigOptions.LOBBY_SERVER.asString()
                  + " wurde nicht gefunden!")
              .color(TextColor.RED));
      return;
    }

    try {
      final RegisteredServer lobbyServer = lobby.get();
      final ServerPing ping = lobbyServer.ping().get(200, TimeUnit.MILLISECONDS);
      issuer.sendMessage(
          TextComponent.of(
              "Du wirst nun mit dem Server "
                  + lobbyServer.getServerInfo().getName()
                  + " verbunden..")
              .color(TextColor.AQUA));
      issuer.createConnectionRequest(lobbyServer).fireAndForget();
    } catch (final InterruptedException | ExecutionException | TimeoutException e) {
      issuer.sendMessage(
          TextComponent.of("Fehler: Der Zielserver ist nicht online.").color(TextColor.RED));
    }
  }
}
