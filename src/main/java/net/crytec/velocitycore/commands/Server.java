package net.crytec.velocitycore.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Optional;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.proxy.server.RegisteredServer;
import com.velocitypowered.api.proxy.server.ServerPing;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import net.kyori.text.TextComponent;
import net.kyori.text.format.TextColor;

@CommandAlias("server")
@CommandPermission("velocity.server")
public class Server extends BaseCommand {

  private final ProxyServer server;

  public Server(final ProxyServer server) {
    this.server = server;
  }

  @Default
  @CommandCompletion("@servers")
  public void execute(final Player issuer, @Optional final RegisteredServer server) {

    try {
      final ServerPing ping = server.ping().get(200, TimeUnit.MILLISECONDS);
      issuer.sendMessage(
          TextComponent.of(
              "Du wirst nun mit dem Server "
                  + server.getServerInfo().getName()
                  + " verbunden..")
              .color(TextColor.AQUA));
      issuer.createConnectionRequest(server).fireAndForget();
    } catch (final InterruptedException | ExecutionException | TimeoutException e) {
      issuer.sendMessage(
          TextComponent.of("Der Server " + server.getServerInfo().getName() + " ist nicht online.")
              .color(TextColor.RED));
    }
  }
}
