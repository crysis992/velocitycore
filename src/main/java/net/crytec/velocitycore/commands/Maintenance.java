package net.crytec.velocitycore.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandIssuer;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Subcommand;
import java.util.List;
import net.crytec.velocitycore.VelocityCore;

@CommandAlias("maintenance")
@CommandPermission("velocity.admin.maintenance")
public class Maintenance extends BaseCommand {

  private final VelocityCore plugin;

  public Maintenance(final VelocityCore plugin) {
    this.plugin = plugin;
  }

  @Subcommand("toggle")
  public void toggle(final CommandIssuer issuer) {
    plugin.getConfig().set("options.maintenance", !plugin.isMaintenanceMode());
    issuer.sendMessage(
        "Der Wartungsmodus ist nun "
            + (plugin.isMaintenanceMode() ? "Aktiviert" : "Deaktiviert")
            + ".");
    plugin.saveConfig();
  }

  @Subcommand("add")
  public void addWhitelist(final CommandIssuer issuer, final String player) {
    final List<String> whitelist = plugin.getConfig().getStringList("maintenance.whitelist");
    if (whitelist.contains(player)) {
      issuer.sendMessage("Dieser Spieler befindet sich bereits auf der Whitelist.");
    } else {
      whitelist.add(player);
      issuer.sendMessage(player + " wurde der Whitelist hinzugefügt.");
      plugin.getConfig().set("maintenance.whitelist", whitelist);
      plugin.saveConfig();
    }
  }

  @Subcommand("remove")
  public void removeWhitelist(final CommandIssuer issuer, final String player) {
    final List<String> whitelist = plugin.getConfig().getStringList("maintenance.whitelist");
    if (!whitelist.contains(player)) {
      issuer.sendMessage("Dieser Spieler befindet sich nicht auf der Whitelist.");
    } else {
      whitelist.remove(player);
      issuer.sendMessage(player + " wurde von der Whitelist entfernt.");
      plugin.getConfig().set("maintenance.whitelist", whitelist);
      plugin.saveConfig();
    }
  }
}
