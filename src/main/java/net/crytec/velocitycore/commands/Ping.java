package net.crytec.velocitycore.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandIssuer;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Optional;
import co.aikar.commands.velocity.contexts.OnlinePlayer;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.ProxyServer;

@CommandAlias("ping")
public class Ping extends BaseCommand {

  private final ProxyServer server;

  public Ping(final ProxyServer server) {
    this.server = server;
  }

  @Default
  @CommandCompletion("@players")
  public void execute(final CommandIssuer sender, @Optional final OnlinePlayer target) {

    if (target != null && sender.hasPermission("ct.ping.others")) {
      sender.sendMessage(
          "Ping von " + target.getPlayer().getUsername() + " ist " + target.getPlayer().getPing());
    } else {
      if (!sender.isPlayer()) {
        sender.sendMessage("§4Dieser Command kann in der Konsole nicht ausgeführt werden!");
        return;
      }
      final long ping = ((Player) sender.getIssuer()).getPing();
      if (ping > 300) {
        sender.sendMessage("§4Dein Ping ist: " + ping + " (Schlecht)");
      } else if (ping > 100) {
        sender.sendMessage("§6Dein Ping ist: " + ping + " (Ok)");
      } else {
        sender.sendMessage("§2Dein Ping ist: " + ping + " (Gut)");
      }
    }
  }
}
