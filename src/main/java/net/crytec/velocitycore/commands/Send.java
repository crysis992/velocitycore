package net.crytec.velocitycore.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Subcommand;
import co.aikar.commands.velocity.contexts.OnlinePlayer;
import com.velocitypowered.api.command.CommandSource;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.proxy.server.RegisteredServer;
import java.util.Collection;
import net.kyori.text.TextComponent;
import net.kyori.text.format.TextColor;

@CommandAlias("send")
@CommandPermission("velocity.send")
public class Send extends BaseCommand {

  private final ProxyServer server;

  public Send(final ProxyServer server) {
    this.server = server;
  }

  @Default
  @CommandCompletion("@players @servers")
  public void sendSinglePlayer(
      final CommandSource issuer, final OnlinePlayer target, final RegisteredServer server) {
    summon(target.getPlayer(), server, issuer);
    issuer.sendMessage(
        TextComponent.of(
            "Du hast " + target.getPlayer().getUsername() + " Spieler erfolgreich verschoben.")
            .color(TextColor.GRAY));
  }

  @Subcommand("all")
  @CommandCompletion("@servers")
  public void sendAllPlayer(final CommandSource issuer, final RegisteredServer server) {
    for (final Player p : server.getPlayersConnected()) {
      summon(p, server, issuer);
    }
    issuer.sendMessage(
        TextComponent.of(
            "Du hast "
                + server.getPlayersConnected().size()
                + " Spieler erfolgreich verschoben.")
            .color(TextColor.GRAY));
  }

  @Subcommand("current")
  @CommandCompletion("@servers")
  public void sendCurrentServer(final Player issuer, final RegisteredServer server) {
    final Collection<Player> currentPlayers =
        issuer.getCurrentServer().get().getServer().getPlayersConnected();
    for (final Player p : currentPlayers) {
      summon(p, server, issuer);
    }
    issuer.sendMessage(
        TextComponent.of("Du hast " + currentPlayers.size() + " Spieler erfolgreich verschoben.")
            .color(TextColor.GRAY));
  }

  private void summon(
      final Player player, final RegisteredServer target, final CommandSource sender) {
    if (player
        .getCurrentServer()
        .get()
        .getServerInfo()
        .getName()
        .equals(target.getServerInfo().getName())) {
      return;
    }
    player.createConnectionRequest(target).fireAndForget();
    player.sendMessage(
        TextComponent.of(
            "§7Du wurdest von auf Server: §6"
                + target.getServerInfo().getName()
                + "§7 verschoben."));
  }
}
