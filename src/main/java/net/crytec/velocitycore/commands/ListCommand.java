package net.crytec.velocitycore.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import com.velocitypowered.api.command.CommandSource;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.ProxyServer;
import java.util.ArrayList;
import java.util.stream.Collectors;
import net.kyori.text.TextComponent;

@CommandAlias("list")
public class ListCommand extends BaseCommand {

  private final ProxyServer proxyServer;

  public ListCommand(final ProxyServer server) {
    proxyServer = server;
  }

  @Default
  public void execute(final CommandSource sender) {

    proxyServer
        .getAllServers()
        .forEach(
            server -> {
              final ArrayList<String> players =
                  new ArrayList<>(
                      server.getPlayersConnected().stream()
                          .map(Player::getUsername)
                          .collect(Collectors.toList()));
              players.sort(String.CASE_INSENSITIVE_ORDER);

              sender.sendMessage(
                  TextComponent.of(
                      "§7[§6"
                          + server.getServerInfo().getName()
                          + "§7]"
                          + " (§6"
                          + players.size()
                          + "§7)§r "
                          + String.join(", ", players)));
            });

    sender.sendMessage(TextComponent.empty());
    sender.sendMessage(TextComponent.of("§7Insgesamt Online: §6" + proxyServer.getPlayerCount()));
  }
}
