package net.crytec.velocitycore.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandIssuer;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Single;
import co.aikar.commands.annotation.Subcommand;
import com.velocitypowered.api.proxy.ProxyServer;
import net.crytec.velocitycore.votifier.Vote;
import net.crytec.velocitycore.votifier.VotifierEvent;

@CommandAlias("velocitydebug")
@CommandPermission("velocity.admin.debug")
public class DebugCommands extends BaseCommand {

  private final ProxyServer server;

  public DebugCommands(final ProxyServer server) {
    this.server = server;
  }

  @Subcommand("vote")
  public void onVoteCommand(
      final CommandIssuer issuer, @Single final String name, final String service) {

    final Vote vote = new Vote();
    vote.setAddress("127.0.0.1");
    vote.setServiceName(service);
    vote.setUsername(name);
    vote.setTimeStamp(String.valueOf(System.currentTimeMillis()));

    server.getEventManager().fireAndForget(new VotifierEvent(vote));
  }
}
