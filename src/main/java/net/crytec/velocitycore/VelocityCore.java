package net.crytec.velocitycore;

import co.aikar.commands.InvalidCommandArgument;
import co.aikar.commands.VelocityCommandManager;
import com.google.inject.Inject;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.event.proxy.ProxyShutdownEvent;
import com.velocitypowered.api.plugin.Dependency;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.proxy.server.RegisteredServer;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.stream.Collectors;
import net.crytec.libs.redishelper.PacketServerType;
import net.crytec.libs.redishelper.RedisManager;
import net.crytec.velocitycore.bans.BanModule;
import net.crytec.velocitycore.commands.Broadcast;
import net.crytec.velocitycore.commands.ListCommand;
import net.crytec.velocitycore.commands.Lobby;
import net.crytec.velocitycore.commands.Maintenance;
import net.crytec.velocitycore.commands.Ping;
import net.crytec.velocitycore.commands.Send;
import net.crytec.velocitycore.commands.Server;
import net.crytec.velocitycore.configuration.file.YamlConfiguration;
import net.crytec.velocitycore.listener.JoinQuit;
import net.crytec.velocitycore.listener.PingListener;
import net.crytec.velocitycore.listener.VoteListener;
import net.crytec.velocitycore.sql.DatabaseManager;
import net.crytec.velocitycore.sql.GlobalDatabase;
import net.crytec.velocitycore.votifier.VoteModule;
import org.slf4j.Logger;

@Plugin(
    id = "velo-core",
    name = "VelocityCore",
    version = "1.0.2",
    description = "A Core Plugin for Velocity",
    authors = {"crysis992"},
    dependencies = {@Dependency(id = "LuckPerms", optional = true)})
public class VelocityCore {

  private final ProxyServer server;
  private final Logger logger;

  private final File pluginFolder = new File("plugins" + File.separator + "VelocityCore");

  private DatabaseManager databaseManager;
  private BanModule banModule;
  private VoteModule voteModule;
  private RedisManager redisManager;

  private YamlConfiguration config;

  @Inject
  public VelocityCore(final ProxyServer server, final Logger logger) {
    this.server = server;
    this.logger = logger;
    if (!pluginFolder.exists() && pluginFolder.mkdir()) {
      logger.info("Created plugin folder");
    }
    try {
      ConfigOptions.initialize(this);
    } catch (final IOException e) {
      e.printStackTrace();
    }
    logger.info("Loading VelocityCore");
  }

  @Subscribe
  public void onProxyInitialization(final ProxyInitializeEvent event) {
    logger.info("Initializing VelocityCore");

    config = YamlConfiguration.loadConfiguration(new File(getDataFolder(), "config.yml"));

    final VelocityCommandManager commandManager = new VelocityCommandManager(server, this);

    commandManager
        .getCommandCompletions()
        .registerCompletion(
            "servers",
            handler -> {
              final String input = handler.getInput();
              return server.getAllServers().stream()
                  .map(s -> s.getServerInfo().getName())
                  .filter(name -> name.startsWith(input))
                  .collect(Collectors.toList());
            });

    commandManager
        .getCommandContexts()
        .registerContext(
            RegisteredServer.class,
            supplier -> {
              final String input = supplier.popFirstArg();
              final Optional<RegisteredServer> server = this.server.getServer(input);

              if (server.isPresent()) {
                return server.get();
              } else {
                throw new InvalidCommandArgument("Es wurde kein Server mit der ID '" + input + "' gefunden.");
              }
            });

    databaseManager = new DatabaseManager(server, this);
    new GlobalDatabase(databaseManager);

    commandManager.registerCommand(new Broadcast(server));
    commandManager.registerCommand(new Server(server));
    commandManager.registerCommand(new Lobby(server));
    commandManager.registerCommand(new Ping(server));
    commandManager.registerCommand(new Send(server));
    commandManager.registerCommand(new ListCommand(server));
//    commandManager.registerCommand(new DebugCommands(server));
    commandManager.registerCommand(new Maintenance(this));

    server.getEventManager().register(this, new JoinQuit(server, this));
    server.getEventManager().register(this, new PingListener(server, this));
    server.getEventManager().register(this, new PluginMessaging(server));
    server.getEventManager().register(this, new VoteListener(this));

    banModule = new BanModule(this, commandManager);

    voteModule = new VoteModule(this, server, logger);

    redisManager = new RedisManager();
    redisManager.setServer("proxy");

    redisManager.registerPacket(PacketServerID.class, "servername", PacketServerType.SPIGOT);
    PacketServerID.server = server;
    PacketServerID.manager = redisManager;
  }

  @Subscribe
  public void onShutdown(final ProxyShutdownEvent event) {
    voteModule.shutdown();
    databaseManager.closePool();
    redisManager.shutdown();
  }

  public File getDataFolder() {
    return pluginFolder;
  }

  public Logger getLogger() {
    return logger;
  }

  public ProxyServer getServer() {
    return server;
  }

  public BanModule getBanModule() {
    return banModule;
  }

  public DatabaseManager getDatabaseManager() {
    return databaseManager;
  }

  public YamlConfiguration getConfig() {
    return config;
  }

  public void saveConfig() {
    getConfig().save(new File(getDataFolder(), "config.yml"));
  }

  public boolean isMaintenanceMode() {
    return getConfig().getBoolean("options.maintenance");
  }
}
