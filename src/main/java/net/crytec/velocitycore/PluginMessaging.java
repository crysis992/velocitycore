package net.crytec.velocitycore;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.connection.PluginMessageEvent;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.proxy.ServerConnection;
import com.velocitypowered.api.proxy.messages.MinecraftChannelIdentifier;
import com.velocitypowered.api.proxy.server.RegisteredServer;
import com.velocitypowered.api.util.UuidUtils;
import java.util.Optional;
import java.util.stream.Collectors;
import net.kyori.text.serializer.legacy.LegacyComponentSerializer;

public class PluginMessaging {

  private static final MinecraftChannelIdentifier MODERN_BUNGEE_CHANNEL =
      MinecraftChannelIdentifier.create("bungeecord", "main");

  private final ProxyServer server;

  public PluginMessaging(final ProxyServer server) {
    this.server = server;
    server.getChannelRegistrar().register(MODERN_BUNGEE_CHANNEL);
  }

  @Subscribe
  public void onPluginMessage(final PluginMessageEvent event) {
    if (!event.getIdentifier().equals(MODERN_BUNGEE_CHANNEL)) {
      return;
    }

    event.setResult(PluginMessageEvent.ForwardResult.handled());

    if (!(event.getSource() instanceof ServerConnection)) {
      return;
    }

    final ServerConnection connection = (ServerConnection) event.getSource();
    final ByteArrayDataInput in = event.dataAsDataStream();
    final ByteArrayDataOutput out = ByteStreams.newDataOutput();
    final String subChannel = in.readUTF();

    if (subChannel.equals("ForwardToPlayer")) {
      server
          .getPlayer(in.readUTF())
          .flatMap(Player::getCurrentServer)
          .ifPresent(
              server -> server.sendPluginMessage(event.getIdentifier(), prepareForwardMessage(in)));
    }
    if (subChannel.equals("Forward")) {
      final String target = in.readUTF();
      final byte[] toForward = prepareForwardMessage(in);

      if (target.equals("ALL")) {
        for (final RegisteredServer rs : server.getAllServers()) {
          rs.sendPluginMessage(event.getIdentifier(), toForward);
        }
      } else {
        server
            .getServer(target)
            .ifPresent(conn -> conn.sendPluginMessage(event.getIdentifier(), toForward));
      }
    }
    if (subChannel.equals("Connect")) {
      final Optional<RegisteredServer> info = server.getServer(in.readUTF());
      info.ifPresent(
          serverInfo -> connection.getPlayer().createConnectionRequest(serverInfo).fireAndForget());
    }
    if (subChannel.equals("ConnectOther")) {
      server
          .getPlayer(in.readUTF())
          .ifPresent(
              player -> {
                final Optional<RegisteredServer> info = server.getServer(in.readUTF());
                info.ifPresent(
                    serverInfo ->
                        connection.getPlayer().createConnectionRequest(serverInfo).fireAndForget());
              });
    }
    if (subChannel.equals("IP")) {
      out.writeUTF("IP");
      out.writeUTF(connection.getPlayer().getRemoteAddress().getHostString());
      out.writeInt(connection.getPlayer().getRemoteAddress().getPort());
    }
    if (subChannel.equals("PlayerCount")) {
      final String target = in.readUTF();
      if (target.equals("ALL")) {
        out.writeUTF("PlayerCount");
        out.writeUTF("ALL");
        out.writeInt(server.getPlayerCount());
      } else {
        server
            .getServer(target)
            .ifPresent(
                rs -> {
                  final int playersOnServer = rs.getPlayersConnected().size();
                  out.writeUTF("PlayerCount");
                  out.writeUTF(rs.getServerInfo().getName());
                  out.writeInt(playersOnServer);
                });
      }
    }
    if (subChannel.equals("PlayerList")) {
      final String target = in.readUTF();
      if (target.equals("ALL")) {
        out.writeUTF("PlayerList");
        out.writeUTF("ALL");
        out.writeUTF(
            server.getAllPlayers().stream()
                .map(Player::getUsername)
                .collect(Collectors.joining(", ")));
      } else {
        server
            .getServer(target)
            .ifPresent(
                info -> {
                  final String playersOnServer =
                      info.getPlayersConnected().stream()
                          .map(Player::getUsername)
                          .collect(Collectors.joining(", "));
                  out.writeUTF("PlayerList");
                  out.writeUTF(info.getServerInfo().getName());
                  out.writeUTF(playersOnServer);
                });
      }
    }
    if (subChannel.equals("GetServers")) {
      out.writeUTF("GetServers");
      out.writeUTF(
          server.getAllServers().stream()
              .map(s -> s.getServerInfo().getName())
              .collect(Collectors.joining(", ")));
    }
    if (subChannel.equals("Message")) {
      final String target = in.readUTF();
      final String message = in.readUTF();
      if (target.equals("ALL")) {
        for (final Player player : server.getAllPlayers()) {
          player.sendMessage(LegacyComponentSerializer.INSTANCE.deserialize(message));
        }
      } else {
        server
            .getPlayer(target)
            .ifPresent(
                player -> {
                  player.sendMessage(LegacyComponentSerializer.INSTANCE.deserialize(message));
                });
      }
    }
    if (subChannel.equals("GetServer")) {
      out.writeUTF("GetServer");
      out.writeUTF(connection.getServerInfo().getName());
    }
    if (subChannel.equals("UUID")) {
      out.writeUTF("UUID");
      out.writeUTF(UuidUtils.toUndashed(connection.getPlayer().getUniqueId()));
    }
    if (subChannel.equals("UUIDOther")) {
      server
          .getPlayer(in.readUTF())
          .ifPresent(
              player -> {
                out.writeUTF("UUIDOther");
                out.writeUTF(player.getUsername());
                out.writeUTF(UuidUtils.toUndashed(player.getUniqueId()));
              });
    }
    if (subChannel.equals("ServerIP")) {
      server
          .getServer(in.readUTF())
          .ifPresent(
              info -> {
                out.writeUTF("ServerIP");
                out.writeUTF(info.getServerInfo().getName());
                out.writeUTF(info.getServerInfo().getAddress().getHostString());
                out.writeShort(info.getServerInfo().getAddress().getPort());
              });
    }
    if (subChannel.equals("KickPlayer")) {
      server
          .getPlayer(in.readUTF())
          .ifPresent(
              player -> {
                final String kickReason = in.readUTF();
                player.disconnect(LegacyComponentSerializer.INSTANCE.deserialize(kickReason));
              });
    }

    // If we wrote data, reply back on the BungeeCord channel
    final byte[] data = out.toByteArray();
    if (data.length > 0) {
      connection.sendPluginMessage(event.getIdentifier(), data);
    }
  }

  private byte[] prepareForwardMessage(final ByteArrayDataInput in) {
    final String channel = in.readUTF();
    final short messageLength = in.readShort();
    final byte[] message = new byte[messageLength];
    in.readFully(message);

    final ByteArrayDataOutput forwarded = ByteStreams.newDataOutput();
    forwarded.writeUTF(channel);
    forwarded.writeShort(messageLength);
    forwarded.write(message);
    return forwarded.toByteArray();
  }
}
