package net.crytec.velocitycore.votifier.thread;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import javax.crypto.BadPaddingException;
import net.crytec.velocitycore.votifier.Vote;
import net.crytec.velocitycore.votifier.VoteModule;
import net.crytec.velocitycore.votifier.VotifierEvent;
import net.crytec.velocitycore.votifier.crypto.RSA;

public class VoteReceiver extends Thread {

  private final VoteModule plugin;

  /**
   * The host to listen on.
   */
  private final String host;

  /**
   * The port to listen on.
   */
  private final int port;

  /**
   * The server socket.
   */
  private ServerSocket server;

  /**
   * The running flag.
   */
  private boolean running = true;

  /**
   * Instantiates a new vote receiver.
   *
   * @param host The host to listen on
   * @param port The port to listen on
   */
  public VoteReceiver(final VoteModule plugin, final String host, final int port) throws Exception {
    this.plugin = plugin;
    this.host = host;
    this.port = port;

    initialize();
  }

  private void initialize() throws Exception {
    try {
      server = new ServerSocket();
      server.bind(new InetSocketAddress(host, port));
    } catch (final Exception ex) {
      VoteModule.LOG.error("Error initializing vote receiver. Please verify that the configured");
      VoteModule.LOG.error("IP address and port are not already in use. This is a common problem");
      VoteModule.LOG.error(
          "with hosting services and, if so, you should check with your hosting provider.", ex);
      throw new Exception(ex);
    }
  }

  /**
   * Shuts the vote receiver down cleanly.
   */
  public void shutdown() {
    running = false;
    if (server == null) {
      return;
    }
    try {
      server.close();
    } catch (final Exception ex) {
      VoteModule.LOG.warn("Unable to shut down vote receiver cleanly.");
    }
  }

  @Override
  public void run() {

    // Main loop.
    while (running) {
      try {
        final Socket socket = server.accept();
        socket.setSoTimeout(5000); // Don't hang on slow connections.
        final BufferedWriter writer =
            new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        final InputStream in = socket.getInputStream();

        // Send them our version.
        writer.write("VOTIFIER " + plugin.getVersion());
        writer.newLine();
        writer.flush();

        // Read the 256 byte block.
        byte[] block = new byte[256];
        in.read(block, 0, block.length);

        // Decrypt the block.
        block = RSA.decrypt(block, plugin.getKeyPair().getPrivate());
        int position = 0;

        // Perform the opcode check.
        final String opcode = readString(block, position);
        position += opcode.length() + 1;
        if (!opcode.equals("VOTE")) {
          // Something went wrong in RSA.
          throw new Exception("Unable to decode RSA");
        }

        // Parse the block.
        final String serviceName = readString(block, position);
        position += serviceName.length() + 1;
        final String username = readString(block, position);
        position += username.length() + 1;
        final String address = readString(block, position);
        position += address.length() + 1;
        final String timeStamp = readString(block, position);
        position += timeStamp.length() + 1;

        // Create the vote.
        final Vote vote = new Vote();
        vote.setServiceName(serviceName);
        vote.setUsername(username);
        vote.setAddress(address);
        vote.setTimeStamp(timeStamp);

        VoteModule.LOG.info("Received vote record -> " + vote);

        // Call event in a synchronized fashion to ensure that the
        // custom event runs in the
        // the main server thread, not this one.

        VoteModule.getInstance().getServer().getEventManager().fire(new VotifierEvent(vote));

        // Clean up.
        writer.close();
        in.close();
        socket.close();
      } catch (final SocketException ex) {
        VoteModule.LOG.warn("Protocol error. Ignoring packet - " + ex.getLocalizedMessage());
      } catch (final BadPaddingException ex) {
        VoteModule.LOG.warn("Unable to decrypt vote record. Make sure that that your public key");
        VoteModule.LOG.warn("matches the one you gave the server list.", ex);
      } catch (final Exception ex) {
        VoteModule.LOG.warn("Exception caught while receiving a vote notification", ex);
      }
    }
  }

  /**
   * Reads a string from a block of data.
   *
   * @param data The data to read from
   * @return The string
   */
  private String readString(final byte[] data, final int offset) {
    final StringBuilder builder = new StringBuilder();
    for (int i = offset; i < data.length; i++) {
      if (data[i] == '\n') {
        break; // Delimiter reached.
      }
      builder.append((char) data[i]);
    }
    return builder.toString();
  }
}
