package net.crytec.velocitycore.votifier;

public class VotifierEvent {

  private final Vote vote;

  public VotifierEvent(final Vote vote) {
    this.vote = vote;
  }

  public Vote getVote() {
    return vote;
  }
}
