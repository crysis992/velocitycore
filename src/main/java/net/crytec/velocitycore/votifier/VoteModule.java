package net.crytec.velocitycore.votifier;

import com.velocitypowered.api.proxy.ProxyServer;
import java.io.File;
import java.security.KeyPair;
import net.crytec.velocitycore.VelocityCore;
import net.crytec.velocitycore.configuration.file.YamlConfiguration;
import net.crytec.velocitycore.votifier.crypto.RSAIO;
import net.crytec.velocitycore.votifier.crypto.RSAKeygen;
import net.crytec.velocitycore.votifier.thread.VoteReceiver;
import org.slf4j.Logger;

public class VoteModule {

  private final ProxyServer server;
  public static Logger LOG;

  private static VoteModule instance;

  private VoteReceiver voteReceiver;
  private KeyPair keyPair;

  public VoteModule(final VelocityCore plugin, final ProxyServer server, final Logger logger) {
    LOG = logger;
    instance = this;
    this.server = server;

    final File rsaDirectory = new File(plugin.getDataFolder() + File.separator + "rsa");
    try {
      if (!rsaDirectory.exists()) {
        if (rsaDirectory.mkdirs()) {
          keyPair = RSAKeygen.generate(2048);
          RSAIO.save(rsaDirectory, keyPair);
        }
      } else {
        keyPair = RSAIO.load(rsaDirectory);
      }
    } catch (final Exception ex) {
      ex.printStackTrace();
    }

    final File config = new File(plugin.getDataFolder(), "votifier.yml");
    if (!config.exists()) {
      try {
        LOG.info("Configuring Votifier for the first time...");
        config.createNewFile();

        final YamlConfiguration configuration = YamlConfiguration.loadConfiguration(config);

        configuration.set("host", "0.0.0.0");
        configuration.set("port", 8192);
        configuration.set("debug", false);

        configuration.save(config);

        LOG.info("------------------------------------------------------------------------------");
        LOG.info("Assigning Votifier to listen on port 8192. If you are hosting Craftbukkit on a");
        LOG.info("shared server please check with your hosting provider to verify that this port");
        LOG.info("is available for your use. Chances are that your hosting provider will assign");
        LOG.info("a different port, which you need to specify in config.yml");
        LOG.info("------------------------------------------------------------------------------");
      } catch (final Exception ex) {
        LOG.error("Error creating configuration file", ex);
        gracefulExit();
        return;
      }
    }

    final YamlConfiguration configuration = YamlConfiguration.loadConfiguration(config);
    try {
      voteReceiver =
          new VoteReceiver(this, configuration.getString("host"), configuration.getInt("port"));
      voteReceiver.start();
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }

  public void shutdown() {
    if (voteReceiver != null) {
      voteReceiver.shutdown();
    }
    LOG.info("VoteReceiver disabled");
  }

  private void gracefulExit() {
    LOG.warn("Votifier did not initialize properly!");
  }

  public static VoteModule getInstance() {
    return instance;
  }

  public ProxyServer getServer() {
    return server;
  }

  public String getVersion() {
    return "1.15.1";
  }

  public KeyPair getKeyPair() {
    return keyPair;
  }
}
