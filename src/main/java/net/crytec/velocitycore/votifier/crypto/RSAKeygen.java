package net.crytec.velocitycore.votifier.crypto;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.spec.RSAKeyGenParameterSpec;
import net.crytec.velocitycore.votifier.VoteModule;

/**
 * An RSA key pair generator.
 */
public class RSAKeygen {

  /**
   * Generates an RSA key pair.
   *
   * @param bits The amount of bits
   * @return The key pair
   */
  public static KeyPair generate(final int bits) throws Exception {
    VoteModule.LOG.info("Votifier is generating an RSA key pair...");
    final KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA");
    final RSAKeyGenParameterSpec spec = new RSAKeyGenParameterSpec(bits, RSAKeyGenParameterSpec.F4);
    keygen.initialize(spec);
    return keygen.generateKeyPair();
  }
}
