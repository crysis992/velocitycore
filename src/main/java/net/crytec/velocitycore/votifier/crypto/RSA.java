package net.crytec.velocitycore.votifier.crypto;

import java.security.PrivateKey;
import java.security.PublicKey;
import javax.crypto.Cipher;

public class RSA {

  /**
   * Encrypts a block of data.
   *
   * @param data The data to encrypt
   * @param key  The key to encrypt with
   * @return The encrypted data
   * @throws Exception If an error occurs
   */
  public static byte[] encrypt(final byte[] data, final PublicKey key) throws Exception {
    final Cipher cipher = Cipher.getInstance("RSA");
    cipher.init(Cipher.ENCRYPT_MODE, key);
    return cipher.doFinal(data);
  }

  /**
   * Decrypts a block of data.
   *
   * @param data The data to decrypt
   * @param key  The key to decrypt with
   * @return The decrypted data
   * @throws Exception If an error occurs
   */
  public static byte[] decrypt(final byte[] data, final PrivateKey key) throws Exception {
    final Cipher cipher = Cipher.getInstance("RSA");
    cipher.init(Cipher.DECRYPT_MODE, key);
    return cipher.doFinal(data);
  }
}
