package net.crytec.velocitycore.votifier.crypto;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * Static utility methods for saving and loading RSA key pairs.
 */
public class RSAIO {

  /**
   * Saves the key pair to the disk.
   *
   * @param directory The directory to save to
   * @param keyPair   The key pair to save
   * @throws Exception If an error occurs
   */
  public static void save(final File directory, final KeyPair keyPair) throws Exception {
    final PrivateKey privateKey = keyPair.getPrivate();
    final PublicKey publicKey = keyPair.getPublic();

    // Store the public and private keys.
    final X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(publicKey.getEncoded());
    final PKCS8EncodedKeySpec privateSpec = new PKCS8EncodedKeySpec(privateKey.getEncoded());

    try (final FileOutputStream publicOut = new FileOutputStream(directory + "/public.key");
        final FileOutputStream privateOut = new FileOutputStream(directory + "/private.key")) {

      publicOut.write(Base64.getEncoder().encode(publicSpec.getEncoded()));
      privateOut.write(Base64.getEncoder().encode(privateSpec.getEncoded()));
    }
  }

  /**
   * Loads an RSA key pair from a directory. The directory must have the files "public.key" and "private.key".
   *
   * @param directory The directory to load from
   * @return The key pair
   * @throws Exception If an error occurs
   */
  public static KeyPair load(final File directory) throws Exception {
    // Read the public key file.
    final File publicKeyFile = new File(directory + "/public.key");
    byte[] encodedPublicKey = Files.readAllBytes(publicKeyFile.toPath());
    encodedPublicKey =
        Base64.getDecoder().decode(new String(encodedPublicKey, StandardCharsets.UTF_8));

    // Read the private key file.
    final File privateKeyFile = new File(directory + "/private.key");
    byte[] encodedPrivateKey = Files.readAllBytes(privateKeyFile.toPath());
    encodedPrivateKey =
        Base64.getDecoder().decode(new String(encodedPrivateKey, StandardCharsets.UTF_8));

    // Instantiate and return the key pair.
    final KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    final X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(encodedPublicKey);
    final PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
    final PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(encodedPrivateKey);
    final PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
    return new KeyPair(publicKey, privateKey);
  }
}
