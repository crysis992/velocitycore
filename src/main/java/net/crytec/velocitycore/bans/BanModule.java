package net.crytec.velocitycore.bans;

import co.aikar.commands.InvalidCommandArgument;
import co.aikar.commands.VelocityCommandManager;
import com.velocitypowered.api.event.PostOrder;
import com.velocitypowered.api.event.ResultedEvent.ComponentResult;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.connection.LoginEvent;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import net.crytec.velocitycore.VelocityCore;
import net.crytec.velocitycore.bans.cache.OfflinePlayer;
import net.crytec.velocitycore.bans.commands.Ban;
import net.crytec.velocitycore.bans.commands.Checkban;
import net.crytec.velocitycore.bans.commands.Kick;
import net.crytec.velocitycore.bans.commands.Tempban;
import net.crytec.velocitycore.bans.commands.Unban;
import net.crytec.velocitycore.bans.data.BanProfile;
import net.crytec.velocitycore.bans.data.BanType;
import net.crytec.velocitycore.bans.sql.DatabaseHandler;
import net.crytec.velocitycore.util.UtilTime;
import net.kyori.text.TextComponent;

public class BanModule {

  private final DatabaseHandler handler;
  private final VelocityCore plugin;

  public BanModule(final VelocityCore plugin, final VelocityCommandManager commandManager) {
    this.plugin = plugin;
    OfflinePlayer.server = plugin.getServer();
    handler = new DatabaseHandler(plugin.getDatabaseManager());
    plugin.getServer().getEventManager().register(plugin, this);

    commandManager
        .getCommandContexts()
        .registerContext(
            OfflinePlayer.class,
            handler -> {
              final String input = handler.popFirstArg();
              final Optional<OfflinePlayer> player = OfflinePlayer.getAccount(input);

              if (player.isPresent()) {
                return player.get();
              } else {
                throw new InvalidCommandArgument(
                    "There is no player with the given input of " + input);
              }
            });

    commandManager
        .getCommandContexts()
        .registerContext(
            TimeUnit.class,
            handler -> {
              final String input = handler.popFirstArg().toUpperCase();
              try {
                return TimeUnit.valueOf(input);
              } catch (final IllegalArgumentException ex) {
                throw new InvalidCommandArgument(
                    "There is no TimeUnit with the given input of " + input);
              }
            });

    commandManager
        .getCommandCompletions()
        .registerCompletion(
            "timeunit",
            handler -> {
              final String input = handler.getInput();
              if (input.isEmpty() || input.isBlank()) {
                return Arrays.stream(TimeUnit.values())
                    .map(TimeUnit::toString)
                    .collect(Collectors.toList());
              }
              return Arrays.stream(TimeUnit.values())
                  .filter(unit -> unit.toString().contains(input.toUpperCase()))
                  .map(TimeUnit::toString)
                  .collect(Collectors.toList());
            });

    commandManager.registerCommand(new Ban(this));
    commandManager.registerCommand(new Unban(this));
    commandManager.registerCommand(new Checkban(this));
    commandManager.registerCommand(new Tempban(this));
    commandManager.registerCommand(new Kick(this));
  }

  @Subscribe(order = PostOrder.FIRST)
  public void onLogin(final LoginEvent event) {
    final Optional<OfflinePlayer> player =
        OfflinePlayer.getAccount(event.getPlayer().getUniqueId().toString());
    player.ifPresent(
        offlinePlayer ->
            plugin
                .getLogger()
                .info(
                    "Loaded Player "
                        + offlinePlayer.getUniqueId()
                        + " "
                        + offlinePlayer.getName()
                        + " into cache!"));
  }

  @Subscribe(order = PostOrder.EARLY)
  public void checkBan(final LoginEvent event) {
    final Optional<OfflinePlayer> player =
        OfflinePlayer.getAccount(event.getPlayer().getUniqueId().toString());
    final Optional<BanProfile> ban = handler.getBanProfile(player.get(), "global");

    final String ip = event.getPlayer().getRemoteAddress().getAddress().getHostAddress();

    if (ban.isPresent()) {
      final BanProfile profile = ban.get();

      if (profile.getType() == BanType.PERMANENT) {
        final TextComponent component =
            TextComponent.builder()
                .content("§4§lDein Zugriff auf diesen Server wurde gesperrt")
                .append("\n\n")
                .append("§fGrund: §6" + profile.getReason())
                .build();
        event.setResult(ComponentResult.denied(component));
      } else if (profile.getType() == BanType.TEMPBAN) {
        if (profile.isExpired()) {
          handler.removeBan(player.get(), "Temporärer Ban abgelaufen.", profile.getServer());
        } else {
          final TextComponent component =
              TextComponent.builder()
                  .content("§4§lDein Zugriff auf diesen Server temporär gesperrt")
                  .append("\n\n")
                  .append("§fGrund: §6" + profile.getReason())
                  .append("\n")
                  .append(
                      "§fLäuft ab in: "
                          + UtilTime.between(LocalDateTime.now(), profile.getExpire()))
                  .build();
          event.setResult(ComponentResult.denied(component));
        }
      }
    }

    if (handler.isIPBanned(ip)) {
      event.setResult(
          ComponentResult.denied(
              TextComponent.of("§4Dein Zugriff auf diesen Server ist gesperrt.")));
    }
  }

  public DatabaseHandler getHandler() {
    return handler;
  }

  public VelocityCore getPlugin() {
    return plugin;
  }
}
