package net.crytec.velocitycore.bans.sql;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import net.crytec.velocitycore.VelocityCore;
import net.crytec.velocitycore.bans.cache.OfflinePlayer;
import net.crytec.velocitycore.bans.data.BanProfile;
import net.crytec.velocitycore.bans.data.BanType;
import net.crytec.velocitycore.sql.DatabaseManager;
import org.jetbrains.annotations.Nullable;

public class DatabaseHandler {

  private static final UUID consoleUUID = UUID.fromString("29ddce14-bb7b-438b-b5b9-14ab98a91de6");

  private final VelocityCore plugin;
  private final DatabaseManager db;
  private final HashBasedTable<UUID, String, BanProfile> cache = HashBasedTable.create();
  private final HashSet<String> ipcache = Sets.newHashSet();
  private final HashMap<UUID, LocalDateTime> mutes = Maps.newHashMap();

  public DatabaseHandler(final DatabaseManager dbManager) {
    db = dbManager;
    plugin = dbManager.getPlugin();
  }

  public boolean isIPBanned(final String ip) {
    if (ipcache.contains(ip)) {
      return true;
    }
    final String sql = "SELECT player FROM bans WHERE ip = ?";

    try (final Connection conn = db.getConnection();
        final PreparedStatement pst = conn.prepareStatement(sql)) {
      pst.setString(1, ip);

      final ResultSet res = pst.executeQuery();

      if (res.next()) {
        final Optional<BanProfile> profile =
            getBanProfile(OfflinePlayer.getAccount(res.getString("player")).get(), "global");
        profile.ifPresent(
            banProfile -> {
              if (!banProfile.getIp().equals("missing")) {
                ipcache.add(ip);
              }
            });
        return true;
      } else {
        return false;
      }
    } catch (final SQLException e) {
      e.printStackTrace();
      return false;
    }
  }

  public Optional<BanProfile> getBanProfile(
      final OfflinePlayer offlinePlayer, @Nullable String server) {
    server = server == null ? "global" : server;
    if (cache.contains(offlinePlayer.getUniqueId(), server)) {
      return Optional.of(cache.get(offlinePlayer.getUniqueId(), server));
    }

    final String sql = "SELECT * FROM bans WHERE player = ?";

    try (final Connection conn = db.getConnection();
        final PreparedStatement pst = conn.prepareStatement(sql)) {
      pst.setString(1, offlinePlayer.getUniqueId().toString());
      final ResultSet res = pst.executeQuery();
      if (res.next()) {

        final long expire = res.getTimestamp("expire").toInstant().toEpochMilli();
        final long issued = res.getTimestamp("timestamp").toInstant().toEpochMilli();

        final BanType type = issued - expire == 0 ? BanType.PERMANENT : BanType.TEMPBAN;

        final UUID id = UUID.fromString(res.getString("player"));

        final OfflinePlayer op = OfflinePlayer.getAccount(id.toString()).get();
        final String nickname = op.getName();

        System.out.println("Resolved UUID -> Name " + op.getName());

        final BanProfile profile =
            new BanProfile(
                nickname,
                id.toString(),
                type,
                res.getTimestamp("expire").toLocalDateTime(),
                res.getString("reason"),
                res.getString("admin"),
                res.getString("server"),
                res.getString("ip"),
                res.getBoolean("silent"));
        cache.put(id, res.getString("server"), profile);
        return Optional.of(profile);
      } else {
        return Optional.empty();
      }

    } catch (final SQLException ex) {
      ex.printStackTrace();
      return Optional.empty();
    }
  }

  public boolean removeBan(
      final OfflinePlayer offlinePlayer, final String reason, final String server) {
    final String effectiveServer = server == null ? "global" : server;
    final String sql = "DELETE FROM bans WHERE player = ? AND server = ?";

    try (final Connection conn = db.getConnection();
        final PreparedStatement pst = conn.prepareStatement(sql)) {

      pst.setString(1, offlinePlayer.getUniqueId().toString());
      pst.setString(2, effectiveServer);
      pst.executeUpdate();
      ipcache.remove(cache.get(offlinePlayer.getUniqueId(), server).getIp());
      cache.remove(offlinePlayer.getUniqueId(), server);

      return true;
    } catch (final SQLException ex) {
      ex.printStackTrace();
      return false;
    }
  }

  public boolean setBanned(
      final OfflinePlayer offlinePlayer,
      final UUID admin,
      final String reason,
      final long duration,
      final TimeUnit unit,
      final boolean silent) {
    return setBanned(offlinePlayer, admin, reason, null, duration, unit, silent);
  }

  public boolean setBanned(
      final OfflinePlayer offlinePlayer,
      final UUID admin,
      final String reason,
      final boolean silent) {
    return setBanned(offlinePlayer, admin, reason, null, 0, TimeUnit.SECONDS, silent);
  }

  public boolean setBanned(
      final OfflinePlayer offlinePlayer,
      final UUID admin,
      final String reason,
      @Nullable final String server,
      final long duration,
      final TimeUnit unit,
      final boolean silent) {
    final LocalDateTime expire = LocalDateTime.now().plusSeconds(unit.toSeconds(duration));
    final String effectiveServer = server == null ? "global" : server;
    final BanType banType = duration == 0 ? BanType.PERMANENT : BanType.TEMPBAN;

    final String sql =
        "INSERT INTO bans (player, ip, timestamp, admin, reason, expire, server, silent) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";

    try (final Connection conn = db.getConnection();
        final PreparedStatement pst = conn.prepareStatement(sql)) {

      final AtomicReference<String> ip = new AtomicReference<>("missing");

      plugin
          .getServer()
          .getPlayer(offlinePlayer.getUniqueId())
          .ifPresent(player -> ip.set(player.getRemoteAddress().getAddress().getHostAddress()));

      pst.setString(1, offlinePlayer.getUniqueId().toString());
      pst.setString(2, ip.get());
      pst.setTimestamp(3, Timestamp.valueOf(LocalDateTime.now()));
      pst.setString(4, admin.toString());
      pst.setString(5, reason);
      pst.setTimestamp(6, Timestamp.valueOf(expire));
      pst.setString(7, effectiveServer);
      pst.setBoolean(8, silent);

      pst.executeUpdate();
      writeHistory();
      return true;

    } catch (final SQLException ex) {
      ex.printStackTrace();
      return false;
    }
  }

  public void writeHistory() throws SQLException {
  }
}
