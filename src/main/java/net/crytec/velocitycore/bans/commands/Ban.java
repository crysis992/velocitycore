package net.crytec.velocitycore.bans.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandIssuer;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import java.util.Optional;
import net.crytec.velocitycore.bans.BanModule;
import net.crytec.velocitycore.bans.cache.OfflinePlayer;
import net.crytec.velocitycore.bans.data.BanProfile;
import net.kyori.text.TextComponent;

@CommandAlias("ban")
@CommandPermission("banmanager.ban")
public class Ban extends BaseCommand {

  private final BanModule plugin;

  public Ban(final BanModule module) {
    plugin = module;
  }

  @Default
  @CommandCompletion("@players")
  public void banPlayer(
      final CommandIssuer issuer, final OfflinePlayer holder, final String reason) {
    final Optional<BanProfile> profile = plugin.getHandler().getBanProfile(holder, "global");

    if (profile.isPresent()) {
      issuer.sendMessage("Spieler " + holder.getName() + " ist bereits gebannt.");
      return;
    }
    if (plugin.getHandler().setBanned(holder, issuer.getUniqueId(), reason, false)) {
      issuer.sendMessage("Spieler " + holder.getName() + " wurde permanent gebannt.");
    } else {
      issuer.sendMessage("§4Es ist etwas schief gelaufen. Ban wurde nicht erstellt.");
    }

    plugin.getPlugin().getServer().getPlayer(holder.getUniqueId()).ifPresent(user -> {
      user.disconnect(TextComponent.of("Du wurdest permanent gebannt."));
    });
  }
}
