package net.crytec.velocitycore.bans.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import com.velocitypowered.api.command.CommandSource;
import java.util.Optional;
import net.crytec.velocitycore.bans.BanModule;
import net.crytec.velocitycore.bans.cache.OfflinePlayer;
import net.crytec.velocitycore.bans.data.BanProfile;
import net.kyori.text.TextComponent;

@CommandAlias("checkban")
@CommandPermission("banmanager.checkban")
public class Checkban extends BaseCommand {

  private final BanModule plugin;

  public Checkban(final BanModule module) {
    plugin = module;
  }

  @Default
  public void execute(final CommandSource issuer, final OfflinePlayer holder) {
    final Optional<BanProfile> profile = plugin.getHandler().getBanProfile(holder, "global");

    if (profile.isPresent()) {
      issuer.sendMessage(TextComponent.of("§6" + holder.getName() + "§4 ist gebannt! (" + profile.get().getReason() + ") "));
    } else {
      issuer.sendMessage(TextComponent.of("§6" + holder.getName() + "§2 ist nicht gebannt."));
    }
  }
}
