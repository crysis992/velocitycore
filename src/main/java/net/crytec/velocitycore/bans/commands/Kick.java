package net.crytec.velocitycore.bans.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandIssuer;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Syntax;
import co.aikar.commands.velocity.contexts.OnlinePlayer;
import net.crytec.velocitycore.bans.BanModule;
import net.kyori.text.TextComponent;

@CommandAlias("kick")
@CommandPermission("banmanager.kick")
public class Kick extends BaseCommand {

  private final BanModule plugin;

  public Kick(final BanModule module) {
    plugin = module;
  }

  @Default
  @CommandCompletion("@players @nothing")
  @Syntax("<Nickname> <Grund>")
  public void unbanPlayer(final CommandIssuer issuer, final OnlinePlayer player, final String reason) {
    player.getPlayer().disconnect(TextComponent.of(reason));
    issuer.sendMessage(player.getPlayer().getUsername() + " wurde gekickt.");
  }
}
