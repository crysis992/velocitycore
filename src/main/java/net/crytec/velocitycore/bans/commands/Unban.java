package net.crytec.velocitycore.bans.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandIssuer;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Syntax;
import java.util.Optional;
import net.crytec.velocitycore.bans.BanModule;
import net.crytec.velocitycore.bans.cache.OfflinePlayer;
import net.crytec.velocitycore.bans.data.BanProfile;

@CommandAlias("unban")
@CommandPermission("banmanager.unban")
public class Unban extends BaseCommand {

  private final BanModule plugin;

  public Unban(final BanModule module) {
    plugin = module;
  }

  @Default
  @CommandCompletion("@nothing @servers")
  @Syntax("<Nickname|UUID> <Server>")
  public void unbanPlayer(
      final CommandIssuer issuer,
      final OfflinePlayer account,
      @Default("global") final String server) {

    final Optional<BanProfile> profile = plugin.getHandler().getBanProfile(account, server);

    if (profile.isEmpty()) {
      issuer.sendMessage("Spieler " + account.getName() + " ist nicht gebannt.");
      return;
    }
    if (plugin.getHandler().removeBan(account, "Account entsperrt.", server)) {
      issuer.sendMessage("Spieler " + account.getName() + " wurde entbannt.");
    }
  }
}
