package net.crytec.velocitycore.bans.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandIssuer;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Syntax;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import net.crytec.velocitycore.bans.BanModule;
import net.crytec.velocitycore.bans.cache.OfflinePlayer;
import net.crytec.velocitycore.bans.data.BanProfile;
import net.kyori.text.TextComponent;

@CommandAlias("tempban")
@CommandPermission("banmanager.tempban")
public class Tempban extends BaseCommand {

  private final BanModule plugin;

  public Tempban(final BanModule module) {
    plugin = module;
  }

  @Default
  @CommandCompletion("@players @nothing @timeunit @nothing")
  @Syntax("<Spieler> <Zeit> <Zeitformat> <Grund>")
  public void execute(
      final CommandIssuer issuer,
      final OfflinePlayer holder,
      final int time,
      final TimeUnit unit,
      final String reason) {
    final Optional<BanProfile> profile = plugin.getHandler().getBanProfile(holder, "global");

    if (profile.isPresent()) {
      issuer.sendMessage("Spieler " + holder.getName() + " ist bereits gebannt.");
      return;
    }

    if (plugin.getHandler().setBanned(holder, issuer.getUniqueId(), reason, time, unit, false)) {
      issuer.sendMessage("Spieler " + holder.getName() + " wurde temporär gebannt.");

      plugin.getPlugin().getServer().getPlayer(holder.getUniqueId()).ifPresent(user -> {
        user.disconnect(TextComponent.of("Du wurdest temporär gebannt."));
      });

    }
  }
}
