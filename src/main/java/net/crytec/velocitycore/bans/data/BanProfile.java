package net.crytec.velocitycore.bans.data;

import java.time.LocalDateTime;

public class BanProfile {

  private final String lastNickname;
  private final String uuid;
  private final BanType type;
  private final LocalDateTime expire;
  private final String reason;
  private final String admin;
  private final boolean silent;
  private final String server;
  private final String ip;

  public BanProfile(
      final String nickname,
      final String uuid,
      final BanType type,
      final LocalDateTime end,
      final String reason,
      final String admin,
      final String server,
      final String ip,
      final boolean silent) {
    lastNickname = nickname;
    this.uuid = uuid;
    this.type = type;
    expire = end;
    this.reason = reason;
    this.admin = admin;
    this.server = server;
    this.silent = silent;
    this.ip = ip;
  }

  public boolean isExpired() {
    if (getType() == BanType.TEMPBAN) {
      return LocalDateTime.now().isAfter(getExpire());
    } else {
      return false;
    }
  }

  public String getLastNickname() {
    return lastNickname;
  }

  public String getUuid() {
    return uuid;
  }

  public BanType getType() {
    return type;
  }

  public LocalDateTime getExpire() {
    return expire;
  }

  public String getReason() {
    return reason;
  }

  public String getAdmin() {
    return admin;
  }

  public boolean isSilent() {
    return silent;
  }

  public String getServer() {
    return server;
  }

  public String getIp() {
    return ip;
  }
}
