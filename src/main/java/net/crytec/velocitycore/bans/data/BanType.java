package net.crytec.velocitycore.bans.data;

public enum BanType {
  TEMPBAN,
  PERMANENT,
  KICK,
  UNBAN,
  MUTE,
  UNMUTE,
  UNKNOWN
}
