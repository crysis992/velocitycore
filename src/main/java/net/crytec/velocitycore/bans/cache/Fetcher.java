package net.crytec.velocitycore.bans.cache;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Optional;
import java.util.UUID;

public class Fetcher {

  public static final long FEBRUARY_2015 = 1422748800000L;
  private static final Gson gson =
      new GsonBuilder().registerTypeAdapter(UUID.class, new UUIDTypeAdapter()).create();

  private static final String UUID_URL = "https://api.mojang.com/users/profiles/minecraft/%s?at=%d";
  private static final String NAME_URL = "https://api.mojang.com/user/profiles/%s/names";

  private String name;
  private UUID id;

  /**
   * Fetches the uuid synchronously and returns it
   *
   * @param name The name
   * @return The uuid
   */
  public Optional<UUID> fetchUUID(final String name) {
    return getUUIDAt(name, System.currentTimeMillis());
  }

  /**
   * Fetches the uuid synchronously for a specified name and time
   *
   * @param name      The name
   * @param timestamp Time when the player had this name in milliseconds
   * @see Fetcher#FEBRUARY_2015
   */
  public Optional<UUID> getUUIDAt(final String name, final long timestamp) {
    try {
      final HttpURLConnection connection =
          (HttpURLConnection)
              new URL(String.format(UUID_URL, name, timestamp / 1000)).openConnection();
      connection.setReadTimeout(2000);
      final Fetcher data =
          gson.fromJson(
              new BufferedReader(new InputStreamReader(connection.getInputStream())),
              Fetcher.class);
      if (data == null || data.id == null) {
        return Optional.empty();
      } else {
        return Optional.of(data.id);
      }
    } catch (final IOException ignored) {
      return Optional.empty();
    }
  }

  /**
   * Fetches the name synchronously and returns it
   *
   * @param uuid The uuid
   * @return The name
   */
  public String fetchName(final UUID uuid) {
    try {
      final HttpURLConnection connection =
          (HttpURLConnection)
              new URL(String.format(NAME_URL, UUIDTypeAdapter.fromUUID(uuid))).openConnection();
      connection.setReadTimeout(2000);
      final Fetcher[] nameHistory =
          gson.fromJson(
              new BufferedReader(new InputStreamReader(connection.getInputStream())),
              Fetcher[].class);
      final Fetcher currentNameData = nameHistory[nameHistory.length - 1];
      return currentNameData.name;
    } catch (final Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
