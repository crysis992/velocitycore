package net.crytec.velocitycore.bans.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.ProxyServer;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import org.jetbrains.annotations.NotNull;

public class OfflinePlayer {

  public static final Fetcher fetcher = new Fetcher();
  public static ProxyServer server;
  private static final Pattern pattern =
      Pattern.compile(
          "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}");

  private static final LoadingCache<String, Optional<OfflinePlayer>> uuidCache =
      CacheBuilder.newBuilder()
          .maximumSize(200)
          .expireAfterWrite(15, TimeUnit.MINUTES)
          .build(new UUIDCacheLoader());

  private static final LoadingCache<UUID, Optional<OfflinePlayer>> nameCache =
      CacheBuilder.newBuilder()
          .maximumSize(200)
          .expireAfterWrite(30, TimeUnit.MINUTES)
          .build(new NameCacheLoader());

  private final UUID uniqueId;
  private final String name;

  public OfflinePlayer(final UUID uuid, final String name) {
    uniqueId = uuid;
    this.name = name;
  }

  public UUID getUniqueId() {
    return uniqueId;
  }

  public String getName() {
    return name;
  }

  private static Optional<String> get(final UUID uuid) throws ExecutionException {
    return nameCache.get(uuid).map(OfflinePlayer::getName);
  }

  private static Optional<UUID> get(final String nickname) throws ExecutionException {
    return uuidCache.get(nickname).map(OfflinePlayer::getUniqueId);
  }

  public static Optional<OfflinePlayer> getAccount(final String string) {
    try {
      return isUUID(string) ? nameCache.get(UUID.fromString(string)) : uuidCache.get(string);
    } catch (final ExecutionException e) {
      e.printStackTrace();
      return Optional.empty();
    }
  }

  private static boolean isUUID(final String string) {
    return pattern.matcher(string).matches();
  }

  private static class NameCacheLoader extends CacheLoader<UUID, Optional<OfflinePlayer>> {

    @Override
    public Optional<OfflinePlayer> load(@NotNull final UUID uuid) {
      final Optional<Player> player = server.getPlayer(uuid);

      if (player.isPresent()) {
        return Optional.of(
            new OfflinePlayer(player.get().getUniqueId(), player.get().getUsername()));
      } else {
        final String name = fetcher.fetchName(uuid);
        return name == null ? Optional.empty() : Optional.of(new OfflinePlayer(uuid, name));
      }
    }
  }

  private static class UUIDCacheLoader extends CacheLoader<String, Optional<OfflinePlayer>> {

    @Override
    public Optional<OfflinePlayer> load(@NotNull final String nickname) {
      final Optional<Player> player = server.getPlayer(nickname);

      if (player.isPresent()) {
        return Optional.of(
            new OfflinePlayer(player.get().getUniqueId(), player.get().getUsername()));
      } else {
        final Optional<UUID> uuid = fetcher.fetchUUID(nickname);
        return uuid.map(value -> new OfflinePlayer(value, nickname));
      }
    }
  }
}
