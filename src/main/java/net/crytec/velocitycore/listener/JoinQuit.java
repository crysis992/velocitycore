package net.crytec.velocitycore.listener;

import com.velocitypowered.api.event.PostOrder;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.connection.DisconnectEvent;
import com.velocitypowered.api.event.connection.PostLoginEvent;
import com.velocitypowered.api.event.connection.PreLoginEvent;
import com.velocitypowered.api.event.connection.PreLoginEvent.PreLoginComponentResult;
import com.velocitypowered.api.event.player.ServerConnectedEvent;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.proxy.player.TabList;
import com.velocitypowered.api.proxy.player.TabListEntry;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import net.crytec.velocitycore.ConfigOptions;
import net.crytec.velocitycore.VelocityCore;
import net.kyori.text.TextComponent;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.cacheddata.CachedMetaData;
import net.luckperms.api.model.user.User;
import net.luckperms.api.model.user.UserManager;
import net.luckperms.api.query.QueryOptions;

public class JoinQuit {

  private final ProxyServer server;
  private final VelocityCore plugin;
  private final LuckPerms api;
  private final UserManager userManager;

  public JoinQuit(final ProxyServer server, final VelocityCore plugin) {
    this.server = server;
    this.plugin = plugin;
    api = LuckPermsProvider.get();
    userManager = api.getUserManager();
  }

  @Subscribe
  public void onLogin(final PostLoginEvent event) {
    final String uuid = event.getPlayer().getUniqueId().toString();
    final String name = event.getPlayer().getUsername();
    final String ip = event.getPlayer().getRemoteAddress().getHostString();

    server.broadcast(TextComponent.of("§7[§2+§7] " + name));
    server
        .getConsoleCommandSource()
        .sendMessage(
            TextComponent.of(
                name
                    + "[§b"
                    + uuid
                    + "§7] [§6"
                    + ip
                    + "§7]"
                    + " joined the server. [§2"
                    + ip
                    + "§7]"));
  }

  @Subscribe
  public void onPreLogin(final PreLoginEvent event) {
    if (!plugin.isMaintenanceMode()) {
      return;
    }

    if (server.getPlayerCount() >= ConfigOptions.MAX_PLAYERS.asInt()) {
      final TextComponent component = TextComponent.builder("§4Dieser Server ist voll!").build();
      event.setResult(PreLoginComponentResult.denied(component));
      return;
    }

    final String username = event.getUsername();
    if (!plugin.getConfig().getStringList("maintenance.whitelist").contains(username)) {
      final TextComponent component =
          TextComponent.builder()
              .content("§4§lACHTUNG!\n\n")
              .append("§4§lUnsere Server sind zur Zeit nicht verfügbar.")
              .append("\n\n")
              .append("§fWir bitten um Geduld, während wir die")
              .append("\n")
              .append("§fWartungsarbeiten durchführen.")
              .append("§fÜblicherweise dauert eine Wartung nicht\n")
              .append("§fmehr als einige Stunden.")
              .append("\n\n")
              .append("§fWeitere Informationen zu dem aktuellen Status\n")
              .append("§ffindest du auf unserer Website.")
              .append("\n\n")
              .append("§fDanke für deine Geduld!")
              .build();
      event.setResult(PreLoginEvent.PreLoginComponentResult.denied(component));
    }
  }

  @Subscribe
  public void onLogin(final ServerConnectedEvent event) {
    if (!ConfigOptions.TABLIST_ENABLED.asBoolean()) {
      return;
    }
    final TabList tab = event.getPlayer().getTabList();

    server
        .getScheduler()
        .buildTask(
            plugin,
            () -> {
              for (TabListEntry entry : tab.getEntries()) {
                UUID player = entry.getProfile().getId();
                String rank = getPlayerPrefix(event.getPlayer());
                String format = ConfigOptions.TABLIST_FORMAT.asString().replace("%prefix%", rank).replace("%playername%", entry.getProfile().getName());
                entry.setDisplayName(TextComponent.of(format));
              }
            })
        .delay(1, TimeUnit.SECONDS)
        .schedule();
  }

  @Subscribe(order = PostOrder.LAST)
  public void onDisconnect(final DisconnectEvent event) {
    if (event.disconnectedDuringLogin()) {
      return;
    }

    final Player player = event.getPlayer();

    final String uuid = player.getUniqueId().toString();
    final String name = player.getUsername();
    final String ip = player.getRemoteAddress().getHostString();

    server.broadcast(TextComponent.of("§7[§c-§7] " + name));
    server
        .getConsoleCommandSource()
        .sendMessage(
            TextComponent.of(name + "[§b" + uuid + "§7] [§6" + ip + "§7] left the server."));
  }

  public String getPlayerPrefix(final Player player) {
    final User user = userManager.getUser(player.getUniqueId());
    if (user == null) {
      plugin.getLogger().error("Failed to retrieve player prefix for user " + player.getUsername());
      return " ";
    }

    final CachedMetaData meta = user.getCachedData().getMetaData(QueryOptions.nonContextual());
    final String prefix = meta.getPrefix();

    return prefix == null ? "" : prefix.replace('&', '§');
  }
}
