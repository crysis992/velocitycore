package net.crytec.velocitycore.listener;

import com.velocitypowered.api.event.Subscribe;
import net.crytec.velocitycore.VelocityCore;
import net.crytec.velocitycore.sql.GlobalDatabase;
import net.crytec.velocitycore.votifier.VotifierEvent;

public class VoteListener {

  private final VelocityCore plugin;

  public VoteListener(final VelocityCore plugin) {
    this.plugin = plugin;
  }

  @Subscribe
  public void onVote(final VotifierEvent event) {
    plugin
        .getLogger()
        .info(event.getVote().getUsername() + " has voted on " + event.getVote().getServiceName());
    GlobalDatabase.get().addVoteRecord(event.getVote());
  }
}
