package net.crytec.velocitycore.listener;

import com.velocitypowered.api.event.PostOrder;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.connection.PreLoginEvent;
import com.velocitypowered.api.event.connection.PreLoginEvent.PreLoginComponentResult;
import com.velocitypowered.api.event.proxy.ProxyPingEvent;
import com.velocitypowered.api.event.query.ProxyQueryEvent;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.proxy.server.ServerPing;
import com.velocitypowered.api.proxy.server.ServerPing.Builder;
import com.velocitypowered.api.proxy.server.ServerPing.Version;
import com.velocitypowered.api.util.Favicon;
import java.io.File;
import java.io.IOException;
import net.crytec.velocitycore.ConfigOptions;
import net.crytec.velocitycore.VelocityCore;
import net.kyori.text.TextComponent;
import net.kyori.text.format.TextColor;

public class PingListener {

  private final ProxyServer server;
  private final VelocityCore plugin;

  private final int PROTOCOL_VERSION;

  private final String motd_line0;
  private final String motd_line1;

  private Favicon icon;

  public PingListener(final ProxyServer server, final VelocityCore plugin) {
    this.plugin = plugin;
    this.server = server;
    try {
      icon = Favicon.create(new File("server-icon.png").toPath());
    } catch (final IOException e) {
      icon = null;
      e.printStackTrace();
    }
    PROTOCOL_VERSION = ConfigOptions.PROTOCOL_VERSION.asInt();

    motd_line0 = ConfigOptions.MOTD.asStringList().get(0).replace('&', '§');
    motd_line1 = ConfigOptions.MOTD.asStringList().get(1).replace('&', '§');
  }

  @Subscribe
  public void onQuery(final ProxyQueryEvent event) {
    event.setResponse(event.getResponse().toBuilder()
        .maxPlayers(ConfigOptions.MAX_PLAYERS.asInt())
        .build());
  }

  @Subscribe
  public void onServerPing(final ProxyPingEvent event) {
    if (plugin.isMaintenanceMode()) {
      event.setPing(
          ServerPing.builder()
              .version(new Version(0, "§4§lWartung"))
              .notModCompatible()
              .onlinePlayers(server.getPlayerCount())
              .maximumPlayers(server.getPlayerCount() + 1)
              .description(TextComponent.of("Wartungsarbeiten...").color(TextColor.DARK_RED))
              .build());
      return;
    }

    final Builder builder = ServerPing.builder();
    builder
        .version(new Version(PROTOCOL_VERSION, ConfigOptions.PROTOCOL_VERSION_NAME.asString()))
        .notModCompatible()
        .onlinePlayers(server.getPlayerCount())
        .maximumPlayers(ConfigOptions.MAX_PLAYERS.asInt())
        .description(
            TextComponent.of(motd_line0 + "\n" + motd_line1).color(TextColor.LIGHT_PURPLE));

    if (icon != null) {
      builder.favicon(icon);
    }

    event.setPing(builder.build());
  }

  @Subscribe(order = PostOrder.FIRST)
  public void onServerPing(final PreLoginEvent event) {
    if (event.getConnection().getProtocolVersion().getProtocol() != PROTOCOL_VERSION) {
      event.setResult(
          PreLoginComponentResult.denied(
              TextComponent.of(
                  "§fLogin fehlgeschlagen! \n\n §cDu hast eine inkompantible Version installiert.\nBitte nutze §e"
                      + ConfigOptions.PROTOCOL_VERSION_NAME.asString()
                      + "\n\n")));
    }
  }
}
