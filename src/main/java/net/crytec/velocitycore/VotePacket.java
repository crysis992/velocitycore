package net.crytec.velocitycore;

import com.google.gson.JsonObject;
import net.crytec.libs.redishelper.NetworkPacket;
import net.crytec.velocitycore.votifier.Vote;

public class VotePacket extends NetworkPacket {

  public VotePacket() {
    vote = null;
  }

  private final Vote vote;

  public VotePacket(final Vote vote) {
    this.vote = vote;
  }

  @Override
  public void onPacketSent(final JsonObject data) {
    data.addProperty("username", vote.getUsername());
    data.addProperty("timestamp", vote.getTimeStamp());
    data.addProperty("service", vote.getServiceName());
  }
}
