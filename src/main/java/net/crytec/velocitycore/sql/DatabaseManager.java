package net.crytec.velocitycore.sql;

import com.velocitypowered.api.proxy.ProxyServer;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.sql.SQLException;
import net.crytec.velocitycore.ConfigOptions;
import net.crytec.velocitycore.VelocityCore;

public class DatabaseManager {

  private final VelocityCore plugin;
  private final ProxyServer server;

  private final HikariDataSource dataSource;

  public DatabaseManager(final ProxyServer server, final VelocityCore plugin) {
    this.plugin = plugin;
    this.server = server;

    final HikariConfig config = new HikariConfig();
    config.setJdbcUrl(
        "jdbc:mysql://"
            + ConfigOptions.MYSQL_HOST.asString()
            + ":"
            + ConfigOptions.MYSQL_PORT.asInt()
            + "/"
            + ConfigOptions.MYSQL_DATABASE.asString());
    config.setDriverClassName("com.mysql.cj.jdbc.Driver");
    config.setUsername(ConfigOptions.MYSQL_USERNAME.asString());
    config.setPassword(ConfigOptions.MYSQL_PASSWORD.asString());
    config.setMinimumIdle(5);
    config.setMaximumPoolSize(5);
    config.setConnectionTimeout(2000);
    config.addDataSourceProperty("cachePrepStmts", "true");
    config.addDataSourceProperty("prepStmtCacheSize", "250");
    config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
    config.addDataSourceProperty("useServerPrepStmts", "true");
    config.setConnectionTestQuery("SELECT 1;");

    dataSource = new HikariDataSource(config);
  }

  public Connection getConnection() throws SQLException {
    return dataSource.getConnection();
  }

  public HikariDataSource getDataSource() {
    return dataSource;
  }

  public void closePool() {
    if (dataSource != null && !dataSource.isClosed()) {
      dataSource.close();
    }
  }

  public VelocityCore getPlugin() {
    return plugin;
  }

  public ProxyServer getServer() {
    return server;
  }
}
