package net.crytec.velocitycore.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import net.crytec.velocitycore.bans.cache.OfflinePlayer;
import net.crytec.velocitycore.votifier.Vote;

public class GlobalDatabase {

  private static GlobalDatabase instance;

  private final DatabaseManager db;

  public GlobalDatabase(final DatabaseManager manager) {
    GlobalDatabase.instance = this;
    db = manager;
  }

  public static GlobalDatabase get() {
    return GlobalDatabase.instance;
  }

  public void addVoteRecord(final Vote vote) {

    final String sql = "INSERT INTO vote_log (player, timestamp, service) VALUES (?, ?, ?);";

    try (final Connection conn = db.getConnection();
        final PreparedStatement pst = conn.prepareStatement(sql)) {

      final AtomicReference<String> ip = new AtomicReference<>("missing");

      final Optional<OfflinePlayer> op = OfflinePlayer.getAccount(vote.getUsername());

      if (op.isEmpty()) {
        db.getPlugin()
            .getLogger()
            .warn(
                vote.getUsername()
                    + " has never played on this server - Unable to insert vote record!");
        conn.close();
        pst.close();
        return;
      }

      pst.setString(1, op.get().getUniqueId().toString());
      pst.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
      pst.setString(3, vote.getServiceName());

      pst.executeUpdate();

    } catch (final SQLException ex) {
      ex.printStackTrace();
    }
  }
}
